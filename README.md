# fumeExtractor / Extractor de humo

Extractor de humo para soldadra de estaño con ventilador reciclado de 120mm de PC.
Materiales:
1.  ventilador de 120mm
2.  cargador li-on tp4056
3.  step up mt3608
4.  bateria li-on 18650 (yo las reciclo de baterias de portatiles)
5.  interruptor de 10x15mm

Smoke extractor for tin welding machine with recycled 120mm PC fan.
Materials:
1. 120mm fan
2. li-on charger tp4056
3.step up mt3608
4. Li-on 18650 battery (I recycle them from laptop batteries)
5.10x15mm switch

